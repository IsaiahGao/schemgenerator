package schem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Directional;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Schematic {
    
    public Schematic(String filenm) {
        this.data = new HashMap<>();
        this.special = new HashMap<>();
        
        File file = new File(Main.instance.getDataFolder() + File.separator + "schematics" + File.separator + filenm);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            
            boolean special = true;
            while (true) {
                String str = reader.readLine();
                if (str == null)
                    break;
                
                if (special && str.equals("##BLOCK SECTION##")) {
                    special = false;
                    continue;
                }
                
                String[] arr = str.split(";");
                if (special) {
                    Vector v = new Vector(Integer.parseInt(arr[1]), Integer.parseInt(arr[2]), Integer.parseInt(arr[3]));
                    this.data.put(v, new MaterialData(Material.AIR, (byte) 0));
                    this.special.put(arr[0], v);
                } else {
                	MaterialData md;
                	
                	try {
                		md = new MaterialData(Material.valueOf(arr[0].toUpperCase()), Byte.parseByte(arr[1]));
                	} catch(IllegalArgumentException e) {
                		md = new MaterialData(Material.valueOf("LEGACY_" + arr[0].toUpperCase()), Byte.parseByte(arr[1]));
                	}
                	
                    this.data.put(new Vector(Integer.parseInt(arr[2]), Integer.parseInt(arr[3]), Integer.parseInt(arr[4])), md);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
    
    private Map<Vector, MaterialData> data;
    private Map<String, Vector> special;
    
    public void place(Location loc, Biome biome) {
        for (final Map.Entry<Vector, MaterialData> entry : data.entrySet()) {
            final Block b = loc.clone().add(entry.getKey()).getBlock();
            
            b.setType(Bukkit.getUnsafe().fromLegacy(entry.getValue()));
            b.setBlockData(Bukkit.getUnsafe().fromLegacy(entry.getValue().getItemType(), entry.getValue().getData()));

            b.setBiome(biome);
        }
    }
    
    public Location getSpecial(String spec, Location center) {
        Vector v = special.get(spec);
        if (v == null)
            return null;
        
        Location loc = center.clone().add(v);
        return loc;
    }

}
