package schem;

import java.util.HashMap;
import java.util.Map;

public class SchematicHandler {
    
    private static Map<String, Schematic> schems = new HashMap<>();
    
    public static Schematic getSchematic(String name) {
        Schematic schem = schems.get(name);
        if (schem != null)
            return schem;
        
        schem = new Schematic(name);
        schems.put(name, schem);
        return schem;
    }

}
