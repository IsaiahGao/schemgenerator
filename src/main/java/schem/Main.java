package schem;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;

public class Main extends JavaPlugin {
    
    public static Main instance;

    @Override
    public void onEnable() {
        instance = this;
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command lbl, String cmd, String[] arr) {
        if (cmd.equalsIgnoreCase("saveschem")) {
            if (!(sender instanceof Player) || !sender.isOp()) {
                return true;
            }
            
            Player p = (Player) sender;
            try {
            	LocalSession ls = WorldEdit.getInstance().getSessionManager().findByName(p.getName());
                Region region = ls.getSelection(ls.getSelectionWorld());
                if (region instanceof CuboidRegion) {
                    Map<Material, String> map = new HashMap<>();
                    for (int i = 1; i < arr.length; i++) {
                        String[] spl = arr[i].split(":");
                        map.put(Material.valueOf(spl[0].toUpperCase()), spl[1]);
                    }
                    
                    new SchematicSaver(arr[0], (CuboidRegion) region, map);
                    p.sendMessage("schematic saved as " + arr[0]);
                    return true;
                }
                p.sendMessage("region isnt cuboid");
            } catch (IncompleteRegionException e) {
                p.sendMessage("region is incomplete");
                return true;
            }
        }
        return false;
    }
    
}
