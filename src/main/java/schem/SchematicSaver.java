package schem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;

public class SchematicSaver {
    
    public SchematicSaver(String name, CuboidRegion region, Map<Material, String> special) {
        this.region = region;
        this.name = name;
        this.special = special;
        try {
            this.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private Map<Material, String> special;
    private CuboidRegion region;
    private String name;
    
    private void save() throws IOException {
        BlockVector3 min = region.getMinimumPoint();
        BlockVector3 max = region.getMaximumPoint();
        List<String> blocks = new ArrayList<>();
        List<String> special = new ArrayList<>();
        
        int minx = min.getBlockX();
        int miny = min.getBlockY();
        int minz = min.getBlockZ();
        
        World world = Bukkit.getWorld(region.getWorld().getName());
        for (int x = min.getBlockX(); x < max.getBlockX(); x++) {
            for (int y = min.getBlockY(); y < max.getBlockY(); y++) {
                for (int z = min.getBlockZ(); z < max.getBlockZ(); z++) {
                    Block b = world.getBlockAt(x, y, z);
                    
                    if (this.special.containsKey(b.getType())) {
                        special.add(this.special.get(b.getType()) + ";" + (x - minx) + ";" + (y - miny) + ";" + (z - minz));
                    } else {
                        blocks.add(b.getType().toString() + ";" + b.getData() + ";" + (x - minx) + ";" + (y - miny) + ";" + (z - minz));
                    }
                }
            }
        }
        
        File dir = new File(Main.instance.getDataFolder() + File.separator + "schematics");
        if (!dir.exists())
            dir.mkdirs();

        FileWriter w = new FileWriter(Main.instance.getDataFolder() + File.separator + "schematics" + File.separator + this.name);
        for (String s : special) {
            w.write(s + System.lineSeparator());
        }
        w.write("##BLOCK SECTION##" + System.lineSeparator());
        for (String s : blocks) {
            w.write(s + System.lineSeparator());
        }
        w.close();
    }

}
